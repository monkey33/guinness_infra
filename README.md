# README #

Utilité de chaque fichier :

- Dans le dossier Playbooks se trouve :
     - playbook_install.yml : Installe le nécessaire sur les serveurs.
     - playbook_preprod.yml : Exécute les requêtes de pull et de déploiement Git de la branche develop
     - playbook_prod.yml : Exécute les requêtes de pull et de déploiement Git de la branche master